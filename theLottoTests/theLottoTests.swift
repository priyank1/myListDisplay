//
//  theLottoTests.swift
//  theLottoTests
//
//  Created by Priyank V. Mandalia on 18/5/18.
//  Copyright © 2018 Priyank V. Mandalia. All rights reserved.
//

import XCTest
@testable import theLotto

class theLottoTests: XCTestCase {
    
    var vc: ViewController!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        vc = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
            vc.downloadTask(bool: false)
        //XCTAssert(true)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
            vc.downloadTask(bool: false)
        }
    }
    
}
