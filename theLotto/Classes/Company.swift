//
//  Companies.swift
//  theLotto
//
//  Created by Priyank V. Mandalia on 18/5/18.
//  Copyright © 2018 Priyank V. Mandalia. All rights reserved.
//

import Foundation

class Company{
    
    var companyDisplayName: String
    var companyId: String
    var companyDescription: String
    var companyLogoUrl: String
    
    init(companyName: String, id: String, companyDescription: String, companyLogoUrl: String) {
        self.companyDisplayName = companyName
        self.companyId = id
        self.companyDescription = companyDescription
        self.companyLogoUrl = companyLogoUrl
        
    }
    
}
