//
//  CompanyCell.swift
//  theLotto
//
//  Created by Priyank V. Mandalia on 18/5/18.
//  Copyright © 2018 Priyank V. Mandalia. All rights reserved.
//

import Foundation
import UIKit

class CompanyCell: UITableViewCell {
    
    @IBOutlet weak var companyLogo: UIImageView!
    @IBOutlet weak var companyDesc: UILabel!
    
    var item: Company? = nil {
        didSet {
            if let item = item {
                companyDesc.text = item.companyDescription

                DispatchQueue.global(qos: .background).async {
                    print("This is run on the background queue")
                    self.companyLogo.downloadedFrom(link: item.companyLogoUrl)
                    
                    DispatchQueue.main.async {
                        print("This is run on the main queue, after the previous code in outer block")
                        
                        self.roundingUIView(aView: self.companyLogo, cornerRadiusParam: 10)
                        
                    }
                }
                
            } else {
                companyDesc.text = ""
                
            }
        }
    }
    
    private func roundingUIView( aView: UIView!, cornerRadiusParam: CGFloat!) {
        
        aView.clipsToBounds = true
        aView.layer.cornerRadius = cornerRadiusParam
        
    }
}

extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        
        let task = URLSession.shared.dataTask(with: url){
            data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.contentMode = mode
                self.image = image
            }
        }
        task.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}
