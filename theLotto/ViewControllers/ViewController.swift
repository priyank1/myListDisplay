//
//  ViewController.swift
//  theLotto
//
//  Created by Priyank V. Mandalia on 18/5/18.
//  Copyright © 2018 Priyank V. Mandalia. All rights reserved.
//

import UIKit

var CompanyList: [Company] = []

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var CompanyTable: UITableView!
    var items = [Company]()
    
    let URL_COMPANIES = "https://api.thelott.com/svc/sales/vmax/web/data/lotto/companies"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        DispatchQueue.global(qos: .background).async {
            print("This is run on the background queue")
            self.downloadTask(bool: true)
            
            DispatchQueue.main.async {
                print("This is run on the main queue, after the previous code in outer block")
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func refresh(_ sender: UIBarButtonItem) {
        
        DispatchQueue.global(qos: .background).async {
            print("This is run on the background queue")
            self.downloadTask(bool: true)
            
            DispatchQueue.main.async {
                print("This is run on the main queue, after the previous code in outer block")
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                
            }
        }
    }
    
    
    func downloadTask(bool: Bool){
        
        //created NSURL
        let requestURL = NSURL(string: URL_COMPANIES)
        
        //creating NSMutableURLRequest
        let request = NSMutableURLRequest(url: requestURL! as URL)
        
        //setting the method to post
        request.httpMethod = "GET"
        
        //creating a task to send the post request
        let task = URLSession.shared.dataTask(with: request as URLRequest){
            data, response, error in
            
            if error != nil{
                print("error is \(error)")
                return;
            }
            else{
                
                // 1. check http response for successful GET request
                if let httpResponse = response as? HTTPURLResponse
                {
                    print("httpResponse status code \(httpResponse.statusCode)")
                    switch(httpResponse.statusCode)
                    {
                    case 200:
                        
                        //parsing the response
                        do {
                            //converting resonse to NSDictionary
                            let myJSON =  try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                            
                            //parsing the json
                            if let parseJSON = myJSON {
                                
                                //getting the json response
                                if let companiesArray = parseJSON["Companies"] as? [[String: AnyObject]] {
                                    
                                        for company in companiesArray {
                                                
                                            let companyId = company["CompanyId"] as? String
                                            let companyDisplayName = company["CompanyDisplayName"] as? String
                                            let companyDescription = company["CompanyDescription"] as? String
                                            let companyLogoUrl = company["CompanyLogoUrl"] as? String
                                            
                                            print("companyDisplayName: "+companyDisplayName!)
                                            print("ID: "+companyId!)
                                            print("companyDescription: "+companyDescription!)
                                            print("companyLogoUrl: "+companyLogoUrl!)
                                        
                                            let company = Company(companyName: companyDisplayName!, id: companyId!, companyDescription: companyDescription!, companyLogoUrl: companyLogoUrl!)
                                            
                                            CompanyList.append(company)
                                            
                                        }
                                 
                                }
                                
                            } //if JSON
                            else{
                                print("Couden't get the JSON properly: 1")
                                
                                DispatchQueue.main.async {
                                    
                                    let alert = UIAlertController(title: "JSON Error", message: "Please try again!", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                    
                                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                    
                                }
                                
                            
                            }
                            
                            
                        } catch {
                            print(error)
                            
                            
                            DispatchQueue.main.async {
                                print("This is run on the main queue, after the previous code in outer block")
                                
                                let alert = UIAlertController(title: "Error", message: "Connection issue!", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                
                            }
                            
                        } //catch
                        
                        
                    default:
                        print("GET resquest not successful. http status code \(httpResponse.statusCode)")
                        
                    }
                    
                }
                
                DispatchQueue.main.async {
                    print("This is run on the main queue, after the previous code in outer block")
                    
                    self.items = CompanyList
                    
                    if(bool){
                    self.CompanyTable.delegate = self
                    self.CompanyTable.dataSource = self
                    self.CompanyTable?.reloadData()
                        
                    }
                    
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    
                }
            }
            
        }// if (error)
        
        task.resume()

        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Item", for: indexPath) as! CompanyCell
        
        let newSwiftColor = UIColor(red: 0.9294, green: 0.9294, blue: 0.9294, alpha:1.0)
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = newSwiftColor
        cell.selectedBackgroundView = bgColorView
        
        let row = indexPath.row
        cell.item = items[row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100.0;//Choose your custom row height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = items[indexPath.row]
        let detailMessage = item.companyDisplayName
        
        DispatchQueue.main.async {
            
            let detailAlert = UIAlertController(title: "Lottery Provider", message: detailMessage, preferredStyle: .alert)
            detailAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(detailAlert, animated: true, completion: nil)
            
        }
        
    }


}

